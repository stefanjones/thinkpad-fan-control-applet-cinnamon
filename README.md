# Thinkpad Fan Control Applet for Cinnamon 

A simple applet (spice) for GUI control of the fan speed on a ThinkPad laptop. Only the most basic functionality is in, with no thermal monitoring, so caution is advised. 

Enabling fan speed control on the thinkpad_acpi module is required for this applet to function. See
[http://www.thinkwiki.org/wiki/How_to_control_fan_speed](http://www.thinkwiki.org/wiki/How_to_control_fan_speed).

*****

The settings available correspond to the fan control levels listed in the wiki article as follows:

**Auto**: auto

**High**: Level 7

**Max**: full-speed / disengaged

*****

To install, copy the **thinkpadfan@stefanjones.ca** folder and all files within into your **~/.local/share/cinnamon/applets/** folder.
