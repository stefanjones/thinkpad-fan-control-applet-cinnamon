const Lang = imports.lang;
const Applet = imports.ui.applet;
const GLib = imports.gi.GLib;
const Gettext = imports.gettext.domain('cinnamon-applets');
const _ = Gettext.gettext;
const Util = imports.misc.util;
const PopupMenu = imports.ui.popupMenu;

/* const Mainloop = imports.mainloop; #DEBUG */

function MyApplet(orientation) {
  this._init(orientation);
}

MyApplet.prototype = {
  __proto__: Applet.TextApplet.prototype,

  _init: function(orientation) {
    Applet.TextApplet.prototype._init.call(this, orientation);
  
    try {
      this.menuManager = new PopupMenu.PopupMenuManager(this);
      this.menu = new Applet.AppletPopupMenu(this, orientation);
      this.menuManager.addMenu(this.menu);            
      
      this.set_applet_label("Fan");
      this.set_applet_tooltip(_("Click to set fan speed"));

      this._maxfan = new PopupMenu.PopupMenuItem(_("Set to Maximum"));
      this._highfan = new PopupMenu.PopupMenuItem(_("Set to High"));
      this._autofan = new PopupMenu.PopupMenuItem(_("Set to Automatic"));

      this._maxfan.connect('activate', Lang.bind(this, this._set_max));
      this._highfan.connect('activate', Lang.bind(this, this._set_high));
      this._autofan.connect('activate', Lang.bind(this, this._set_auto));

      this.menu.addMenuItem(this._maxfan);
      this.menu.addMenuItem(this._highfan);
      this.menu.addMenuItem(this._autofan);    

      /* Mainloop.timeout_add(5000, Lang.bind(this, this._update_data)); */
    } 
    catch (e) {
      global.logError(e);
    }

  },

  on_applet_clicked: function(event) {
      this.menu.toggle();        
  },
  
  /* A single process needs to be started to control the fan. */
  /* gksudo needs to be used (in sudo mode) to prompt for a password via the GUI */    
  _set_auto: function() {

    try {
      GLib.spawn_command_line_async("gksudo -S \"sh -c 'echo level auto | sudo tee /proc/acpi/ibm/fan'\"");
      this.set_applet_label("Auto");
    } catch (e) {
      global.logError(e);
    }
  },

  _set_high: function() {
    try {
      GLib.spawn_command_line_async("gksudo -S \"sh -c 'echo level 7 | sudo tee /proc/acpi/ibm/fan'\"");
      this.set_applet_label("High");
    } catch (e) {
      global.logError(e);
    }
  },

  _set_max: function() {
    try {
      GLib.spawn_command_line_async("gksudo -S \"sh -c 'echo level full-speed | sudo tee /proc/acpi/ibm/fan'\"");
      this.set_applet_label("Max");
    } catch (e) {
      global.logError(e);
    }
  },

  _update_data: function() {
    /*this.set_applet_label("Fan");*/
  }
};

function main(metadata, orientation) {
  let myApplet = new MyApplet(orientation);
  return myApplet;
}
